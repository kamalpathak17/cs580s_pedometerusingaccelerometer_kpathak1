package com.example.pedometer;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    // Display fields for Accelerometer
    private TextView textViewX;
    private TextView textViewY;
    private TextView textViewZ;

    //Display field for Sensitivity
    private TextView textSensitive;

    //Display for Steps
    private TextView textViewSteps;

    //Reset Button
    private Button buttonReset;

    //Sensor Manager
    private SensorManager sensorManager;
    private float acceleration;

    //Values to calculate Number of Steps
    private float previousY;
    private float currentY;
    private int numSteps;

    //SeekBar Fields
    private SeekBar seekBar;
    private int threshold; //Point at which we trigger and count it as a step

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Attach objects to XML view
        textViewX = (TextView) findViewById(R.id.textViewX);
        textViewY = (TextView) findViewById(R.id.textViewY);
        textViewZ = (TextView) findViewById(R.id.textViewZ);

        //Attach step and sensitive view objects to XML
        textViewSteps = (TextView) findViewById(R.id.textSteps);
        textSensitive = (TextView) findViewById(R.id.textSensitive);

        //Attach the reset button to XML
        buttonReset = (Button) findViewById(R.id.buttonReset);

        //Attach the seekBar to XML
        seekBar = (SeekBar) findViewById(R.id.seekBar);

        //Set the values on the seekbar, threshold, and threshold display
        seekBar.setProgress(10);
        seekBar.setOnSeekBarChangeListener(seekBarListener);
        threshold = 10;
        textSensitive.setText(String.valueOf(threshold));

        //Initializing Values
        previousY = 0;
        currentY = 0;
        numSteps = 0;

        //Initialize Acceleration Values
        acceleration = 0.00f;

        //Enable the Listener
        enableAccelerometer();
    }

    private void enableAccelerometer()
    {
        //Initialize the Sensor Manager
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),SensorManager.SENSOR_DELAY_NORMAL);
    }

    //Event handler for accelerometer events
    private SensorEventListener sensorEventListener = new SensorEventListener() {

        //Listens for change in Acceleration, Displays, and compute the Steps
        @Override
        public void onSensorChanged(SensorEvent event) {

            //Gather the values from Accelerometer
            float X = event.values[0];
            float Y = event.values[1];
            float Z = event.values[2];

            //Fetch the current Y
            currentY = Y;

            //Measure if a step is taken
            if( Math.abs(currentY - previousY) > threshold)
            {
                numSteps++;
                textViewSteps.setText(String.valueOf(numSteps));
            }

            //Display the Values
            textViewX.setText(String.valueOf(X));
            textViewY.setText(String.valueOf(Y));
            textViewZ.setText(String.valueOf(Z));

            //Store the previous Y
            previousY = Y;

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    }; //Ends private innerClass sensorEventListener

    //Called by ResetSteps button to reset the number of steps
    public void resetSteps(View V)
    {
        numSteps = 0;
        textViewSteps.setText(String.valueOf(numSteps));
    }

    //innerclass for the seekBarListener
    private SeekBar.OnSeekBarChangeListener seekBarListener= new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            //Change the threshold
            threshold = seekBar.getProgress();

            //Write to text View
            textSensitive.setText(String.valueOf(threshold));
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    }; //Ends InnerClass seekBarListener

}
